% nodes = [0 0;nan nan;36 0;0 36;36 36;72 36;nan nan];%无效的节点变为nan(表示已删除)
% element = [3 1;4 3;4 5;3 5;3 6;6 5];%每个单元两端对应两节点 
% F = [0 0;0,0;0 0;0 0;0 -500; 0 -500];%1列Fx， 2列Fy
% Dis = [1 1; 0,0;0 0;1 1]; %1 表示限制约束；1列x方向 2列y方向
% area = [8 8 8 8 8 8];%每个杆件截面积
% E = 1960000;
% 
% OUT_Dis = GetDis(nodes,element,F,Dis,area,E);
function OUT_Dis = GetDis(nodes,element,F,Dis,area,E, Nodes_number)
    m = size(nodes,1);
    KEach = zeros(2*m,2*m);
    KSum = zeros(2*m,2*m);
    %计算合成刚度矩阵
    for i = 1:size(element(:,1))
        node1_number =find(Nodes_number == element(i,1));
        node2_number = find(Nodes_number == element(i,2));
        if ~isnan(node1_number)
            %有效的单元
            node1 = nodes(node1_number,:);
            node2 = nodes(node2_number,:);
            inc_x = node2(1) - node1(1);
            inc_y = node2(2) - node1(2);
            %杆件单元的角度转到全局坐标
            L = norm(node1-node2); 
            a = atan(inc_y / inc_x);
            T = area(i) * E / L *...
               [cos(a)^2, sin(a)*cos(a), -cos(a)^2 ,-sin(a)*cos(a);
                sin(a)*cos(a), sin(a)^2, -sin(a)*cos(a), -sin(a)^2;
                -cos(a)^2, -sin(a)*cos(a), cos(a)^2, sin(a)*cos(a);
                -sin(a)*cos(a),  -sin(a)^2, sin(a)*cos(a), sin(a)^2];

            for o=1:4
                for p = 1:4
                    if abs(T(o,p)) < 0.00001
                        T(o,p) = 0;
                    end
                end
            end
           %该单元刚度矩阵放入对应位置，其余为0，便于直接相加单刚
           KEach(2*node1_number-1:2*node1_number,2*node1_number-1:2*node1_number) = T(1:2,1:2);
           KEach(2*node1_number-1:2*node1_number,2*node2_number-1:2*node2_number) = T(1:2,3:4);
           KEach(2*node2_number-1:2*node2_number,2*node1_number-1:2*node1_number) = T(3:4,1:2);
           KEach(2*node2_number-1:2*node2_number,2*node2_number-1:2*node2_number) = T(3:4,3:4);  
        end 
        %叠加单元刚度矩阵
        KSum = KSum + KEach;
        KEach = zeros(2*m,2*m);
    end
    %后面的K是总体刚度矩阵
    K = KSum;
    %F变为 1列
    F_New = TwoColToOne(F,2*m);
    %Dis 位移约束变为1列
    Dis_New = TwoColToOne(Dis,2*m);

    newK = [];
    newF = [];
    newDis = [];
    %去除无效的即被删除的行
    for i = 1:2*m
        if mod(i,2) == 1
            va = nodes((i-1)/2+1,1);
        end
        if mod(i,2) == 0
            va = nodes(i/2,2);
        end
        if ~isnan(va)
            newK =[newK; K(i,:)] ;
            newF = [newF; F_New(i)];
            newDis = [newDis; Dis_New(i)];
        end 
    end
    %总体刚度矩阵还要去除列
    K = newK;
    newK = [];
    for i = 1:2*m
        if mod(i,2) == 1
            va = nodes((i-1)/2+1,1);
        end
        if mod(i,2) == 0
            va = nodes(i/2,2);
        end
        if ~isnan(va)
            newK =[newK,K(:,i)] ;
        end 
    end

    %利用边界条件再次去除行，K还多去除列
    %得到最终的K，F
    K = [];
    F = [];
    for i = 1:size(newDis,1)
        if newDis(i) ~= 1
            K = [K; newK(i,:)];
            F = [F; newF(i,:)];
        end
    end  
    newK = K;
    K = [];
    for i = 1:size(newDis,1)
        if newDis(i) ~= 1
            K = [K, newK(:,i)];
        end
    end  

    %计算利用边界条件后的位移
    Dis = K \  F;
    %添加边界条件处的[0 0]
    DisResult = [];
    n = 1;
    for i = 1:size(newDis,1)/2
        if newDis(2*i) ~= 1
            DisResult = [DisResult; [Dis(n), Dis(n+1)]];
            n = n + 2;
        else
            DisResult = [DisResult; [0,0]];
        end
    end  

    %对应上节点序号
    n = 1;
    for i = 1:size(nodes,1)
        if ~isnan(nodes(i,1))
            DisResult(n,3) = Nodes_number(i);
            n = n+1;
        end
    end
    OUT_Dis = DisResult;
    n = 1;
end
function OUT_newMat = TwoColToOne(IN_Mat, IN_num)
    OUT_newMat = zeros(IN_num,1);
    for i = 1:size(IN_Mat,1)
        if IN_Mat(i,1) ~= 0
            OUT_newMat(2*i-1) = IN_Mat(i,1);     
        end  
        if IN_Mat(i,2) ~= 0
            OUT_newMat(2*i) = IN_Mat(i,2);     
        end 
    end
end